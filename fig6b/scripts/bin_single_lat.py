import pandas as pd
import sys
import numpy as np

def main():
    if len(sys.argv) != 3:
        print("Usage: python bin_single_lat.py <filename> <out_name>")
        sys.exit(1)
    filename = sys.argv[1]
    outfile = sys.argv[2]
    df = pd.read_csv(filename)
    df['start_rel'] = (df.Start - np.min(df.Start)) / 1e9
    df["start_rel"] = df["start_rel"].astype(int)
    filtered_df = df.groupby(df["start_rel"]).agg({'Duration':"mean"})
    filtered_df.to_csv(outfile)

if __name__ == '__main__':
    main()

