#!/bin/bash

source ../../ae_ktr/env.sh
# Run CPU interference for 25 seconds
# Note that this file should be placed in the anomaly-injector directory of FIRM
cd $FIRM_ANOMALY_DIR
echo $(date +%s%N) > meta2_ts_22000_25_75.txt
./cpu 25
