import pandas as pd
import sys
import numpy as np

def main():
    if len(sys.argv) != 3:
        print("Usage: python start_time.py <filename> <outfile>")
        sys.exit(1)
    filename = sys.argv[1]
    outfile = sys.argv[2]
    df = pd.read_csv(filename)
    start_time = np.min(df.Start)
    with open(outfile, 'w') as outf:
        outf.write(str(start_time))

if __name__ == '__main__':
    main()
