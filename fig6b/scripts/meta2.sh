#!/bin/bash

source ../../ae_data/env.sh
mkdir -p ../ae_data
$BLUEPRINT_DIR/wrk_http -config=$BLUEPRINT_EXP_DIR/common/hotel_blueprint.json -tput=22000 -duration=10m -outfile=../ae_data/meta2_hotel_blueprint_22000_75.csv &
sleep 5m
echo "Run Now"
sleep 5m
python start_time.py ../ae_data/meta2_hotel_blueprint_22000_75.csv ../ae_data/meta2_trigger_start_22000_75.txt
python bin_single_lat.py ../ae_data/meta2_hotel_blueprint_22000_75.csv ../ae_data/meta2_trigger_hotel_blueprint_22000_75.csv