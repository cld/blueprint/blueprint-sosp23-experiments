# 6.2.1 Type 2 Metastability Failure

In this experiment, we reproduce figure 6b from the accepted version of the paper.

## Prerequisites

Before you run any experiments for figure 6b, we would like to remind you to setup your environment and machines according to the information in the Kick-the-tires Readme.

## Generating + Running Systems

To generate the system, on $MACHINE2, run the [generate_hr.sh](./scripts/generate_hr.sh) script in the scripts directory.

Execute the script as follows

```
> ./scripts/generate_hr.sh
```

To deploy the system, on $MACHINE2, execute the following steps.

```
> cd $BLUEPRINT_SYSTEMS_DIR/blueprint-dsb-hotelreservation/output_go_sospae_fig6b
> sudo docker compose build
> sudo docker compose up -d
```

To run the workload, on $MACHINE1, run the [meta2.sh](./scripts/meta2.sh) script in the scripts directory.

Execute the instructions as follows

```
> cd scripts
> ./meta2.sh
```

At the 5 minute mark, the script will print out a message saying "Run Now" at which point, on $MACHINE2, you must run the [meta2_anomaly.sh](./scripts/meta2_anomaly.sh) to inject CPU interference that will act as a trigger and cause the GC and main threads to compete.

This will create a timestamp file called ```meta2_ts_2200025_75.txt``` that you should move from $MACHINE2 to $MACHINE1 in the `ae_data` folder of this experiment.

## Plotting Results

To plot the newly generated results, we have provided a Jupyter notebook. Currently the Jupyter notebook uses the data that we had generated for the original experiments. To view the generated results, modify the variable called `DATA_DIR` such that its value is now `ae_data/` instead of `data/`. After this, you can re-run all the cells in the Jupyter notebook.