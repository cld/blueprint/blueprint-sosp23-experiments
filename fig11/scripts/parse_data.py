import pandas as pd
import numpy as np
import sys

class ExperimentRun:
    def __init__(self) -> None:
        self.avg_latency = 0
        self.avg_tput = 0
        self.target_tput = 0
        self.std_latency = 0

def main():
    if len(sys.argv) != 5:
        print("Usage: python parse_data.py data_dir prefix_name max_tput_num config_name")
        sys.exit(1)

    data_dir = sys.argv[1]
    prefix_name = sys.argv[2]
    tput_num = int(sys.argv[3]) + 1000
    config_name = sys.argv[4]
    
    experiments = []
    for i in range(1000, tput_num, 1000):
        df = pd.read_csv(data_dir + "/" + prefix_name + "_" + str(i) + ".csv")
        min_val = df["Start"].min()
        df["start_fixed"] = (df["Start"] - min_val) / 1e9
        df["end"] = df["start_fixed"] + (df["Duration"]/1e9)
        df = df.drop(df[df.IsError == True].index)
        df = df.drop(df[df.end < 1].index)
        df = df.drop(df[df.end > 59].index)
        filtered_df = df.groupby(pd.cut(df["end"], np.arange(1,59,1))).agg({'end':'size', 'Duration':'mean'})
        filtered_df = filtered_df.replace(np.nan, 0)
        avg_latency = filtered_df['Duration'].mean() / 1e6
        avg_tput = filtered_df['end'].mean()
        std_latency = df['Duration'].std() / 1e6
        e = ExperimentRun()
        e.avg_latency = avg_latency
        e.avg_tput = avg_tput
        e.std_latency = std_latency
        e.target_tput = i
        experiments += [e]

    experiment_df = pd.DataFrame([e.__dict__ for e in experiments])
    experiment_df.to_csv(data_dir + "/" + prefix_name + "_" + config_name + "_consolidated.csv")

if __name__ == '__main__':
    main()
