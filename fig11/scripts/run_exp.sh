#!/bin/bash

source ../../ae_ktr/env.sh
mkdir -p ../ae_data
for i in $(eval echo {$1..$2..$3})
do
	$BLUEPRINT_DIR/wrk_http -config=$BLUEPRINT_EXP_DIR/common/$4.json -tput=$i -outfile=../ae_data/$4_$i.csv
done
