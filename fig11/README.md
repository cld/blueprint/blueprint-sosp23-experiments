# 6.4 System Comparison w/ Originals

## Hotel Reservation

### Blueprint Version

To generate the system, on $MACHINE2, please use the [./scripts/generate_hr.sh](./scripts/generate_hr.sh).

Execute the script as follows:

```
> ./scripts/generate_hr_grpc.sh
```

#### Deploying the system

To deploy the system, on $MACHINE2, execute the following steps

```
> cd $BLUEPRINT_SYSTEMS_DIR/blueprint-dsb-hotelreservation/output_go_sospae_fig11
> sudo docker compose build
> sudo docker compose up -d
```

Now run the full workload script from $MACHINE1

```
> cd scripts
> ./run_hr.sh
```

### Original Version

To build and deploy the original version on $MACHINE2, execute the following steps.

```
> cd $BLUEPRINT_SYSTEMS_DIR/DeathStarBench/hotelReservation
> sudo docker compose build
> sudo docker compose up -d
```

Now run the full workload script from $MACHINE1

```
> cd scripts
> ./run_hr_original.sh
```

## Social Network

### Blueprint Version

To generate the system, on $MACHINE2, please use the [./scripts/generate_sn.sh](./scripts/generate_sn.sh).

Execute the script as follows:

```
> ./scripts/generate_sn_grpc.sh
```

#### Deploying the system

To deploy the system, on $MACHINE2, execute the following steps

```
> cd $BLUEPRINT_SYSTEMS_DIR/blueprint-dsb-hotelreservation/output_go_sospae_fig11
> sudo docker compose build
> sudo docker compose up -d
```

#### Running the workload

To run the necessary workload from $MACHINE1, execute the [run_sn_grpc.sh](./scripts/run_sn_grpc.sh) in the scripts folder.

```
> cd scripts
> ./run_sn_grpc.sh
```

#### Running the workload

Run the script called [init_social_graph.sh](../common/init_social_graph.sh) from $MACHINE1 to populate the user database.

Now run the full workload script

```
> cd scripts
> ./run_sn.sh
```

### Original Version

To build and deploy the original version on $MACHINE2, execute the following steps.

```
> cd $BLUEPRINT_SYSTEMS_DIR/DeathStarBench/socialNetwork
> sudo docker compose build
> sudo docker compose up -d
```

#### Running the workload

Run the script called [init_social_graph_original.sh](../common/init_social_graph_original.sh) from $MACHINE2 to populate the user database.

Now run the full workload script

```
> cd scripts
> ./run_sn_original.sh
```

## Plotting Results

To plot the newly generated results, we have provided a Jupyter notebook. Currently the Jupyter notebook uses the data that we had generated for the original experiments. To view the generated results, modify the variable called `DATA_DIR` such that its value is now `ae_data/` instead of `data/`. After this, you can re-run all the cells in the Jupyter notebook.