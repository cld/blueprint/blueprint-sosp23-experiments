#!/bin/bash

source ../ae_ktr/env.sh

echo "Plugin,Compiler(LoC),Stdlib(LoC)"
var="$(wc -l < $BLUEPRINT_DIR/generators/plugin_retry.go)"
echo "Retry,$var,0"
var="$(wc -l < $BLUEPRINT_DIR/generators/plugin_tracer.go)"
var2="$(wc -l < $BLUEPRINT_DIR/stdlib/components/tracer.go)"
echo "Tracing,$var,$var2"
var="$(wc -l < $BLUEPRINT_DIR/generators/plugin_platform_replication.go)"
echo "p-Replication,$var,0"
var="$(wc -l < $BLUEPRINT_DIR/generators/plugin_clientpool.go)"
var2="$(wc -l < $BLUEPRINT_DIR/stdlib/clientpool.go)"
echo "ClientPools,$var,$var2"
var="$(wc -l < $BLUEPRINT_DIR/generators/plugin_xtrace.go)"
var2="$(wc -l < $BLUEPRINT_DIR/stdlib/components/xtrace.go)"
var3="$(wc -l < $BLUEPRINT_DIR/stdlib/choices/tracer/xtrace.go)"
res=$((var2+var3))
echo "X-Trace,$var,$res"
var="$(wc -l < $BLUEPRINT_DIR/generators/plugin_circuit_breaker.go)"
echo "CircuitBreaker,$var,0"
var="$(wc -l < $BLUEPRINT_DIR/generators/plugin_loadbalancer.go)"
var2="$(wc -l < $BLUEPRINT_DIR/stdlib/loadbalancer.go)"
echo "LoadBalancer,$var,$var2"