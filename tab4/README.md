# 6.5 Adding new plugins

To re-generate Table 4, simply run the included script [print_table.sh](./print_table.sh). Upon running the script, you should
see the following output

```
Plugin,Compiler(LoC),Stdlib(LoC)
Retry,120,0
Tracing,280,44
p-Replication,60,0
ClientPools,156,56
X-Trace,376,67
CircuitBreaker,131,0
LoadBalancer,213,18
```

Note, that the lines of code don't exactly match the Table included in the paper as there have been improvements in the code since the SOSP submission deadline.