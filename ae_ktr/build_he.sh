#!/bin/bash

source ./env.sh

cd $BLUEPRINT_DIR
go build ./cmd/blueprint
mkdir -p examples/SOSP_AE_KickTheTires/output_go
./blueprint -config=examples/SOSP_AE_KickTheTires/input/config_go.json
