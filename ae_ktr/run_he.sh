#!/bin/bash

source ./env.sh

cd $BLUEPRINT_DIR/examples/SOSP_AE_KickTheTires/output_go
sudo docker compose build
sudo docker compose up -d
