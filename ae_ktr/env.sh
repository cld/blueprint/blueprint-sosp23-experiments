#!/bin/bash

export BLUEPRINT_DIR=/local/blueprint/blueprint-compiler
export BLUEPRINT_SYSTEMS_DIR=/local/blueprint/blueprint-systems-test
export BLUEPRINT_EXP_DIR=/local/blueprint/blueprint-sosp23-experiments
export FIRM_ANOMALY_DIR=/local/firm/anomaly-injector

export MACHINE1_IP=swsnetlab07
export MACHINE2_IP=swsnetlab09