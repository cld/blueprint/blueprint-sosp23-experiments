#!/bin/bash

source ./env.sh

mkdir -p $BLUEPRINT_SYSTEMS_DIR/blueprint-dsb-socialnetwork/output_go_sospae_fig5_grpc
mkdir -p $BLUEPRINT_SYSTEMS_DIR/blueprint-dsb-socialnetwork/output_go_sospae_fig5_monolith
mkdir -p $BLUEPRINT_SYSTEMS_DIR/blueprint-dsb-socialnetwork/output_go_sospae_fig5_thrift512
mkdir -p $BLUEPRINT_SYSTEMS_DIR/blueprint-dsb-socialnetwork/output_go_sospae_fig5_thrift4096
mkdir -p $BLUEPRINT_SYSTEMS_DIR/blueprint-dsb-socialnetwork/output_go_sospae_fig6d
mkdir -p $BLUEPRINT_SYSTEMS_DIR/blueprint-dsb-socialnetwork/output_go_sospae_fig8_no_repl
mkdir -p $BLUEPRINT_SYSTEMS_DIR/blueprint-dsb-socialnetwork/output_go_sospae_fig8_repl
mkdir -p $BLUEPRINT_SYSTEMS_DIR/blueprint-dsb-socialnetwork/output_go_sospae_fig9
mkdir -p $BLUEPRINT_SYSTEMS_DIR/blueprint-dsb-socialnetwork/output_go_sospae_fig11

mkdir -p $BLUEPRINT_SYSTEMS_DIR/blueprint-dsb-hotelreservation/output_go_sospae_fig5_grpc
mkdir -p $BLUEPRINT_SYSTEMS_DIR/blueprint-dsb-hotelreservation/output_go_sospae_fig5_monolith
mkdir -p $BLUEPRINT_SYSTEMS_DIR/blueprint-dsb-hotelreservation/output_go_sospae_fig5_thrift512
mkdir -p $BLUEPRINT_SYSTEMS_DIR/blueprint-dsb-hotelreservation/output_go_sospae_fig5_thrift4096
mkdir -p $BLUEPRINT_SYSTEMS_DIR/blueprint-dsb-hotelreservation/output_go_sospae_fig6a
mkdir -p $BLUEPRINT_SYSTEMS_DIR/blueprint-dsb-hotelreservation/output_go_sospae_fig6b
mkdir -p $BLUEPRINT_SYSTEMS_DIR/blueprint-dsb-hotelreservation/output_go_sospae_fig6c
mkdir -p $BLUEPRINT_SYSTEMS_DIR/blueprint-dsb-hotelreservation/output_go_sospae_fig7_5
mkdir -p $BLUEPRINT_SYSTEMS_DIR/blueprint-dsb-hotelreservation/output_go_sospae_fig7_10
mkdir -p $BLUEPRINT_SYSTEMS_DIR/blueprint-dsb-hotelreservation/output_go_sospae_fig10
mkdir -p $BLUEPRINT_SYSTEMS_DIR/blueprint-dsb-hotelreservation/output_go_sospae_fig11
