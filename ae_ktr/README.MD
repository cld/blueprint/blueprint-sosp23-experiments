# SOSP Artifact Evaluation Kick-the-Tires

As part of the AE Kick-The-Tires period, we expect the evaluators to install all the necessary dependencies
and set up the environment for performing the reproduction.

## Installation Instructions

### Installing Go

To install Go, please follow the instructions [here](https://go.dev/doc/install). Note that for our experiments, we used Go 1.18 for the original experiments but we have successfully used the compiler with Go 1.20.6, the latest version at the time of the writing of this readme.
Note that blueprint will not work for any versions lower than Go 1.18 as blueprint uses generics that were only introduced in Go 1.18.

### Installing Python

We require that you use python3.8. Note that, any other version of python hasn't been tested with Blueprint and could lead to unforeseen issues.

We recommend installing python3.8 using [Anaconda](https://docs.anaconda.com/free/anaconda/install/linux/) and then creating a new python 3.8 environment.
To do so, execute the following instructions

```
> conda install -c anaconda python=3.8
> conda create -n py38 python=3.8
> source activate py38
> python --version
python 3.8.x # Output
```

Then you can install additional libraries for python using the following command

```
> pip install astunparse
```

Plotting results of experiments requires using jupyter. If you installed python using anaconda then you should have jupyter already installed.
If not then execute the following command to install jupyter

```
> pip install jupyter
```

### Installing GRPC for Go

To install grpc for Go, please follow the instructions [here](https://grpc.io/docs/languages/go/quickstart/)

### Installing Thrift

To install thrift, you can either use our prepared installation script called [install_thrift.sh](./install_thrift.sh) or manually do the installation.
We suggest the first option.

To execute the script, simply do the following

```
> ./install_thrift.sh
```

#### Manual Installation

If you have already installed thrift using our auto-installation script then you can completely skip this step.

To install the prerequisites for installing Apache Thrift, please follow the instructions [here](https://thrift.apache.org/docs/install/debian.html).

Download the Apache thrift compiler from [here](https://thrift.apache.org/docs/install/debian.html). Blueprint expects v0.16.0 for its usage.

To build and install from source, please follow the instructions [here](https://thrift.apache.org/docs/BuildingFromSource).

### Installing Docker

To install docker, please follow the instructions [here](https://docs.docker.com/desktop/install/linux-install/). Note, that the experiment scripts heavily rely on ```docker compose```, so please check that the command is correctly installed. You may need ```sudo``` access to install and use docker on your machine.

### Installing FIRM

Our evaluation uses [Firm's](https://www.usenix.org/conference/osdi20/presentation/qiu) anomaly injector to inject interference needed for producing Type 2 and Type 3 Metastability Failures.
To install the anomaly injector, please use the [install_firm.sh](./install_firm.sh) script that we have provided.

Please execute the following command

```
> ./install_firm.sh
```

### Environmental Setup

To setup the environment for using the blueprint compiler, clone the blueprint-compiler repository and set the environment variable ```BLUEPRINT_DIR``` to the location where blueprint-compiler is located.

For example, if the blueprint-compiler repo was located at ```/home/user/blueprint-compiler/```, then set the environment variable as follows

```
> export BLUEPRINT_DIR=/home/user/blueprint-compiler
```

Please modify the [env.sh](./env.sh) script to set the environment variable for correct use in automated scripts.

#### Other Environment Variables

env.sh script has other environment variables that also need to be set for use by other automated scripts. The list of environment variables are:

+ BLUEPRINT_DIR: Path to blueprint-compiler.
+ BLUEPRINT_SYSTEMS_DIR: Path to the location where the systems will be downloaded.
+ BLUEPRINT_EXP_DIR: Path to the location of this repository.
+ FIRM_ANOMALY_DIR: Path to the location to FIRM's anomaly-injector.
+ MACHINE1_IP: IP address or Hostname of the machine where the client workloads will be executed.
+ MACHINE2_IP: IP address or Hostname of the machine where the docker containers will be executed.

The current env.sh file contains examples of the values for each of the environment variables. Please replace them with the values for your environment.

You also need to set the ulimit of your machine to a high number otherwise the workload generator might work. Increase the ulimit by executing the following command

```
> sudo ulimit -n 131072
```

#### Fixing configuration files

Config files in the repo need to be fixed up to use the environment variables that are just defined.
This can be done by running a simple script called [rewrite_json.sh](../common/rewrite_json.sh).
To run this script, do the following

```
> cd $BLUEPRINT_EXP_DIR/common
> ./rewrite_json.sh
```

### Building Blueprint

To build the compiler, please execute the following commands.

```
> cd $BLUEPRINT_DIR
> git checkout v0.1.0 # Checkout the specific tag for AE
> go mod download
> go build ./cmd/blueprint
> go build ./cmd/wrk_http
> go build ./cmd/xsconsistency
```

This will generate a binary file titled `blueprint` that will be used for all the experiments.

## Testing Instructions

To test that Blueprint has been successfully built, we have developed a baby microservice application called SOSP_AE_KickTheTires.

### Building SOSP_AE_KickTheTires

To build the SOSP_AE_KickTheTires application, you can either use the included script called [build_he.sh](./build_he.sh) or perform the following manual instructions.

```
> cd $BLUEPRINT_DIR
> go build ./cmd/blueprint
> mkdir examples/SOSP_AE_KickTheTires/output_go
> ./blueprint -config=examples/SOSP_AE_KickTheTires/input/config_go.json
```

This will generate the SOSP_AE_KickTheTires application that will run 1 service at port 5500. 

To deploy the application, please run the [run_he.sh](./run_he.sh) script or execute the following commands manually.

```
> cd $BLUEPRINT_DIR/examples/SOSP_AE_KickTheTires/output_go
> sudo docker compose build
> sudo docker compose up -d
```

This was launch the application. To, inspect that the application is correctly running, please execute the following command

```
> sudo docker ps
```

You should see the following output:
![image](./bp-docker-ps-output.png)

Now you can make request to the application using cUrl and verify that you see the expected output as shown below.

```
> curl http://localhost:5500/KickTheTires

{"Ret0":"The tires are kicking! Ready for Artifact Evaluation! :)"}
```

If the outputs match, then you have successfully completed the KickTheTires section and are ready to move on to reproducing experiments.

## Downloading systems

To download all the systems used for the evaluation, please run the script [download_systems.sh](../tab1/download_systems.sh). Note that using this script to download the systems is important otherwise automated scripts might not work correctly.

### Generating empty output folders

To generate the empty output folders, please run the script [create_output_directories.sh](./create_output_directories.sh). This will generate the empty folders where the generated code from the various experiment folders will be placed. Note, that this is required otherwise the system generation step will fail.