# 6.1 Performance-Driven Design Exploration

## Prerequsites

Before you run any experiments for figure 5, we would like to remind you to setup your environment and machines according to the information in the Kick-the-tires Readme.

## Experiment Details

For this experiment, we will be using 2 applications with 4 variant implementations each. For each application, the section is divided into 2 parts - generating the variant system and running the system with the necessary workload.

### DSB-HotelReservation

### Variant1 : grpc

#### Generating the system

To generate the system, on $MACHINE2, please use the [./scripts/generate_hr_grpc.sh](./scripts/generate_hr_grpc.sh).

Execute the script as follows:

```
> ./scripts/generate_hr_grpc.sh
```

#### Deploying the system

To deploy the system, on $MACHINE2, execute the following steps

```
> cd $BLUEPRINT_SYSTEMS_DIR/blueprint-dsb-hotelreservation/output_go_sospae_fig5_grpc
> sudo docker compose build
> sudo docker compose up -d
```

#### Running the workload

To run the necessary workload from $MACHINE1, execute the [run_hr_grpc.sh](./scripts/run_hr_grpc.sh) in the scripts folder.

```
> cd scripts
> ./run_hr_grpc.sh
```

### Variant2 : thrift-512

#### Generating the system

To generate the system, on $MACHINE2, please use the [./scripts/generate_hr_thrift512.sh](./scripts/generate_hr_thrift512.sh).

Execute the script as follows:

```
> ./scripts/generate_hr_thrift512.sh
```

#### Deploying the system

To deploy the system, on $MACHINE2, execute the following steps

```
> cd $BLUEPRINT_SYSTEMS_DIR/blueprint-dsb-hotelreservation/output_go_sospae_fig5_thrift512
> sudo docker compose build
> sudo docker compose up -d
```

#### Running the workload

To run the necessary workload from $MACHINE1, execute the [run_hr_thrift512.sh](./scripts/run_hr_thrift512.sh) in the scripts folder.

```
> cd scripts
> ./run_hr_grpc.sh
```

### Variant3 : thrift-4096

#### Generating the system

To generate the system, on $MACHINE2, please use the [./scripts/generate_hr_thrift4096.sh](./scripts/generate_hr_thrift4096.sh).

Execute the script as follows:

```
> ./scripts/generate_hr_thrift4096.sh
```


#### Deploying the system

To deploy the system, on $MACHINE2, execute the following steps

```
> cd $BLUEPRINT_SYSTEMS_DIR/blueprint-dsb-hotelreservation/output_go_sospae_fig5_thrift4096
> sudo docker compose build
> sudo docker compose up -d
```

#### Running the workload

To run the necessary workload from $MACHINE1, execute the [run_hr_thrift4096.sh](./scripts/run_hr_thrift4096.sh) in the scripts folder.

```
> cd scripts
> ./run_hr_grpc.sh
```

### Variant4 : monolith

#### Generating the system

To generate the system, on $MACHINE2, please use the [./scripts/generate_hr_monolith.sh](./scripts/generate_hr_monolith.sh).

Execute the script as follows:

```
> ./scripts/generate_hr_monolith.sh
```


#### Deploying the system

To deploy the system, on $MACHINE2, execute the following steps

```
> cd $BLUEPRINT_SYSTEMS_DIR/blueprint-dsb-hotelreservation/output_go_sospae_fig5_monolith
> sudo docker compose build
> sudo docker compose up -d
```

#### Running the workload

To run the necessary workload from $MACHINE1, execute the [run_hr_monolith.sh](./scripts/run_hr_monolith.sh) in the scripts folder.

```
> cd scripts
> ./run_hr_grpc.sh
```

### DSB-SocialNetwork

### Variant1 : grpc

#### Generating the system

To generate the system, on $MACHINE2, please use the [./scripts/generate_sn_grpc.sh](./scripts/generate_sn_grpc.sh).

Execute the script as follows:

```
> ./scripts/generate_sn_grpc.sh
```

#### Deploying the system

To deploy the system, on $MACHINE2, execute the following steps

```
> cd $BLUEPRINT_SYSTEMS_DIR/blueprint-dsb-hotelreservation/output_go_sospae_fig5_grpc
> sudo docker compose build
> sudo docker compose up -d
```

#### Running the workload

To run the necessary workload from $MACHINE1, execute the [run_sn_grpc.sh](./scripts/run_sn_grpc.sh) in the scripts folder.

```
> cd scripts
> ./run_sn_grpc.sh
```

### Variant2 : thrift-512

#### Generating the system

To generate the system, on $MACHINE2, please use the [./scripts/generate_sn_thrift512.sh](./scripts/generate_sn_thrift512.sh).

Execute the script as follows:

```
> ./scripts/generate_sn_thrift512.sh
```

#### Deploying the system

To deploy the system, on $MACHINE2, execute the following steps

```
> cd $BLUEPRINT_SYSTEMS_DIR/blueprint-dsb-hotelreservation/output_go_sospae_fig5_thrift512
> sudo docker compose build
> sudo docker compose up -d
```

#### Running the workload

To run the necessary workload from $MACHINE1, execute the [run_sn_thrift512.sh](./scripts/run_sn_thrift512.sh) in the scripts folder.

```
> cd scripts
> ./run_sn_grpc.sh
```

### Variant3 : thrift-4096

#### Generating the system

To generate the system, on $MACHINE2, please use the [./scripts/generate_sn_thrift4096.sh](./scripts/generate_sn_thrift4096.sh).

Execute the script as follows:

```
> ./scripts/generate_sn_thrift4096.sh
```


#### Deploying the system

To deploy the system, on $MACHINE2, execute the following steps

```
> cd $BLUEPRINT_SYSTEMS_DIR/blueprint-dsb-hotelreservation/output_go_sospae_fig5_thrift4096
> sudo docker compose build
> sudo docker compose up -d
```

#### Running the workload

To run the necessary workload from $MACHINE1, execute the [run_sn_thrift4096.sh](./scripts/run_sn_thrift4096.sh) in the scripts folder.

```
> cd scripts
> ./run_sn_grpc.sh
```

### Variant4 : monolith

#### Generating the system

To generate the system, on $MACHINE2, please use the [./scripts/generate_sn_monolith.sh](./scripts/generate_sn_monolith.sh).

Execute the script as follows:

```
> ./scripts/generate_sn_monolith.sh
```


#### Deploying the system

To deploy the system, on $MACHINE2, execute the following steps

```
> cd $BLUEPRINT_SYSTEMS_DIR/blueprint-dsb-hotelreservation/output_go_sospae_fig5_monolith
> sudo docker compose build
> sudo docker compose up -d
```

#### Running the workload

To run the necessary workload from $MACHINE1, execute the [run_sn_monolith.sh](./scripts/run_sn_monolith.sh) in the scripts folder.

```
> cd scripts
> ./run_sn_grpc.sh
```

## Plotting Results

To plot the newly generated results, we have provided a Jupyter notebook. Currently the Jupyter notebook uses the data that we had generated for the original experiments. To view the generated results, modify the variable called `DATA_DIR` such that its value is now `ae_data/` instead of `data/`. After this, you can re-run all the cells in the Jupyter notebook.
