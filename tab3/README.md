# 6.5 Adding new Instantiations

To re-generate Table 3, simply run the included script [print_table.sh](./print_table.sh). Upon running the script, you should
see the following output

```
Type,Instantiation,Impl(LoC),Compiler(LoC)
Cache,Redis,77,86
Cache,Memcached,101,86
NoSQLDB,MongoDB,283,86
RelDB,MySQL,90,86
Queue,RabbitMQ,51,112
Tracer,Jaeger,28,87
Tracer,Zipkin,28,87
Deployer,Docker,73,0
Deployer,Kubernetes,44,0
Deployer,Ansible,430,0
RPC,GRPC,711,0
RPC,Thrift,670,0
HTTP,Go's net/http,292,0
```

Note that the lines of code don't exactly match the Table included in the paper as there have been improvements in the code since the SOSP submission deadline.