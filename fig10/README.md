# 6.3 Prototyping New Solutions

In this experiment, we reproduce figure 10 from the accepted version of the paper.

## Prerequisites

Before you run any experiments for figure 10, we would like to remind you to setup your environment and machines according to the information in the Kick-the-tires Readme. In addition, it would be a good idea to run the fig6a experiment although that is not a strict requirement. Ideally, one should compare the results from the experiment run in fig6a with the results of the experiment run in this experiment. However, you can also simply compare against the stored data.

## Generating + Running Systems

To generate the system, on $MACHINE2, run the [generate_hr.sh](./scripts/generate_hr.sh) script in the scripts directory.

Execute the script as follows

```
> ./scripts/generate_hr.sh
```

To deploy the system, on $MACHINE2, execute the following steps.

```
> cd $BLUEPRINT_SYSTEMS_DIR/blueprint-dsb-hotelreservation/output_go_sospae_fig10
> sudo docker compose build
> sudo docker compose up -d
```

To run the workload, on $MACHINE1, run the [meta1.sh](./scripts/meta1.sh) script in the scripts directory.

Execute the instructions as follows

```
> cd scripts
> ./meta1.sh
```

## Plotting Results

To plot the newly generated results, we have provided a Jupyter notebook. Currently the Jupyter notebook uses the data that we had generated for the original experiments. To view the generated results, modify the variable called `DATA_DIR` such that its value is now `ae_data/` instead of `data/`. After this, you can re-run all the cells in the Jupyter notebook.