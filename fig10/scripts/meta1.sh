#!/bin/bash

source ../../ae_ktr/env.sh
mkdir -p ../ae_data
# Depending on the machine this throughput might need to change.
TRIGGER_TPUT=30000
BASE_TPUT=10000
$BLUEPRINT_DIR/wrk_http -config=$BLUEPRINT_EXP_DIR/hotel_blueprint.json -tput=$BASE_TPUT -duration=1m -outfile=../ae_data/meta1_hotel_blueprint_10000_1.csv &
sleep 60
$BLUEPRINT_DIR/wrk_http -config=$BLUEPRINT_EXP_DIR/hotel_blueprint.json -tput=$TRIGGER_TPUT -duration=20s -outfile=../ae_data/meta1_hotel_blueprint_30000_3.csv &
sleep 20
$BLUEPRINT_DIR/wrk_http -config=$BLUEPRINT_EXP_DIR/hotel_blueprint.json -tput=$BASE_TPUT -duration=30s -outfile=../ae_data/meta1_hotel_blueprint_10000_2.csv
