import pandas as pd
import sys
import os
import numpy as np

def q99(x):
    x.quantile(0.99)

def main():
    if len(sys.argv) != 4:
        print("Usage: python parse_vuln_data.py filename_prefix outdir fixed_param")
        sys.exit(1)
    tputs = [10000, 12000, 14000, 16000, 18000, 20000, 22000, 24000, 26000, 28000]
    durations = [0, 5, 10, 15, 20, 25, 30]
    file_prefix = sys.argv[1]
    outdir = sys.argv[2]
    param = sys.argv[3]
    for t in tputs:
        for d in durations:
            filename = file_prefix + "_" + str(t) + "_" + str(d) + "_" + param + ".csv"
            if not os.path.exists(filename):
                continue
            df = pd.read_csv(filename)
            start_time = np.min(df['Start'])
            df['start_rel'] = (df.Start - start_time) / 1e9
            df['start_rel'] = df['start_rel'].astype(int)
            df['Duration']  = df['Duration'] / 1e6
            filtered_df = df.groupby(df['start_rel']).agg(start_rel=('start_rel', 'mean'), avg_lat=('Duration', 'mean'), p99_lat=('Duration', q99))
            outfilename = os.path.join(outdir, "meta3_vuln_" + str(t) + "_" + str(d) + "_" + param + ".csv")
            filtered_df.to_csv(outfilename)

if __name__ == '__main__':
    main()
