#!/bin/bash

echo $(date +%s%N) > meta3_ts_$1_$2.txt
# Run CPU interference for 30 seconds
# Note that this file should be placed in the anomaly-injector directory of FIRM
./cpu $1
