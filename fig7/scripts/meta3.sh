#!/bin/bash

source ../../ae_ktr/env.sh

$BLUEPRINT_DIR/wrk_http -config=$BLUEPRINT_EXP_DIR/hotel_blueprint.json -tput=$1 -duration=2m -outfile=../ae_data/meta3_hotel_blueprint_$1_$2_$3.csv &
sleep 55
echo "Run Now"
