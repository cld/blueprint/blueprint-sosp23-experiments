# 6.2.1 Type 3 Metastability Failure Vulnerability Analysis

In this experiment, we reproduce figure 7 from the accepted version of the paper.

## Prerequisites

Before you run any experiments for figure 7, we would like to remind you to setup your environment and machines according to the information in the Kick-the-tires Readme. Additionally, we recommend running the experiment in the figure 6c before you attempt at running this as this experiment requires running 120 different versions of the experiment in figure 6.

We recommend that in the interests of saving the time of the evaluators, they do not try to run this experiment as part of the AE process.

## Generating + Running Systems

We use 2 different variants for this experiment.

### Variant 1: Num_Retries=5

To generate the system, on $MACHINE2, run the [generate_hr_5.sh](./scripts/generate_hr_5.sh) script in the scripts directory.

To deploy the system, on $MACHINE2, execute the following steps.

```
> cd $BLUEPRINT_SYSTEMS_DIR/blueprint-dsb-hotelreservation/output_go_sospae_fig7_10
> sudo docker compose build
> sudo docker compose up -d
```

### Variant 2: Num_Retries=10

To generate the system, on $MACHINE2, run the [generate_hr_10.sh](./scripts/generate_hr_10.sh) script in the scripts directory.

Execute the script as follows

```
> ./scripts/generate_hr_10.sh
```

To deploy the system, on $MACHINE2, execute the following steps.

```
> cd $BLUEPRINT_SYSTEMS_DIR/blueprint-dsb-hotelreservation/output_go_sospae_fig7_10
> sudo docker compose build
> sudo docker compose up -d
```

### Running Workload

For each variant we will try 10 different workloads and 7 different trigger combinations.
This gives rise to a total of 70 different experimental runs for each variant.

To run the workload, on $MACHINE1, run the [meta3.sh](./scripts/meta3.sh) script in the scripts directory.

Execute the instructions as follows

```
> cd scripts
> ./meta3.sh tput_value trigger_duration fixed_param
```

For variant1 the values will be as follows

```
> ./meta3.sh {10000, 12000, 14000, 16000, 18000, 20000, 22000, 24000, 26000, 28000} {0,5,10,15,20,25,30} 5 
```

For variant2 the values will be as follows

```
> ./meta3.sh {10000, 12000, 14000, 16000, 18000, 20000, 22000, 24000, 26000, 28000} {0,5,10,15,20,25,30} 10 
```

At the 55 second mark, the script will print out a message saying "Run Now" at which point, on $MACHINE2, you must run the [meta3_anomaly.sh](./scripts/meta3_anomaly.sh) to inject CPU interference that will act as a trigger and cause the GC and main threads to compete.

Run the [generate_results.sh](./generate_results.sh) script to generate the files to be used by jupyter.

## Plotting Results

To plot the newly generated results, we have provided a Jupyter notebook. Currently the Jupyter notebook uses the data that we had generated for the original experiments. To view the generated results, modify the variable called `DATA_DIR` such that its value is now `ae_data/` instead of `data/`. After this, you can re-run all the cells in the Jupyter notebook.

To generate the final graph, you would have manually analyze the generated graphs to figure out if a certain configuration had a metastable failure or not. Edit the meta_retry_10y and meta_retry_5y variables according to your assessment and generate the corresponding graph.