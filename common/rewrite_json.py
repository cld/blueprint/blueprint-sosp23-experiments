import json
import sys
import os

def main():

    file = sys.argv[1]

    blueprint_sys_dir = os.environ["BLUEPRINT_SYSTEMS_DIR"]
    machine2 = os.environ["MACHINE2_IP"]
    with open(file, "r") as jsonFile:
        data = json.load(jsonFile)

    if "src_dir" in data:
        data["src_dir"] = data["src_dir"].replace("/local/vaastav/blueprint-systems", blueprint_sys_dir)
    if "output_dir" in data:
        data["output_dir"] = data["output_dir"].replace("/local/vaastav/blueprint-systems", blueprint_sys_dir)
    if "wiring_file" in data:
        data["wiring_file"] = data["wiring_file"].replace("/local/vaastav/blueprint-systems", blueprint_sys_dir)

    if "url" in data:
        data["url"] = data["url"].replace("swsnetlab09", machine2)

    with open(file, "w") as jsonFile:
        json.dump(data, jsonFile, indent=4)

if __name__ == '__main__':
    main()