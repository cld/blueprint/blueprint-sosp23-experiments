#!/bin/bash

source ../../ae_ktr/env.sh
cd $FIRM_ANOMALY_DIR
echo $(date +%s%N) > meta3_24000_ts.txt
# Run CPU interference for 30 seconds
# Note that this file should be placed in the anomaly-injector directory of FIRM
./cpu 30
