import random
import pandas as pd
import numpy as np

def sampling(losses, window_size, samp_prob, use_alpha=True, return_type="both"):
    """ Function to perform the sampling and returns sampled traces 
        and/or sampling probability for each trace.

        Params:
            losses: list of losses for all traces
            window_size: size of window to store most recently seen traces
            samp_prob: sampling probability
            use_alpha: alpha is used to keep the number of samples close to the desired percentage
            return_type: set the returning type (samples, probabilities or both)

        Returns:
            if return_type == "prob", returns list of sampling probabilities
            if return_type == "samples", returns list of sampled traces
            if return_type == "both", returns both
    """

    sampled_traces, sampling_probabilities, window = [], [], []

    for (idx, l) in enumerate(losses):

        # Check if reached window size 
        if len(window) == window_size:
            window.pop(0)
        window.append(l)

        if idx == 0:
            continue

        sampled_ideal = idx*samp_prob
        sampled_actual = len(sampled_traces)
        alpha = (2**(sampled_ideal - sampled_actual)) if use_alpha else 1    
        prob =  ( (l-np.min(window)) / sum_window(window) ) * len(window) * samp_prob * alpha

        if prob or prob == 0:
            # Sample trace based on given probability
            if decision(prob):
                sampled_traces.append(idx)
            sampling_probabilities.append(prob)
    
    if return_type == "prob":
        return sampling_probabilities
    elif return_type == "samples":
        return sampled_traces
    elif return_type == "both":
        return (sampled_traces, sampling_probabilities)
    
    return "Specify a valid return type"

def decision(probability):
    return random.random() < probability

def sum_window(window):
    sum = 0
    min = np.min(window)
    for i in window:
        sum += (i-min)
    return sum