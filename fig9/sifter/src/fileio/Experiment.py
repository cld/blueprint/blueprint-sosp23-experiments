from os import listdir, makedirs
from os.path import isdir, isfile, exists
from graph.Trace import Trace

class Experiment:

    def __init__(self, directory):
        self.directory = directory
        self.filetype = "json"
        self.traces_per_type = {}
        self.type_per_trace = {}
        self._find_files() 

    def get_trace_types(self):
        return self.traces_per_type.keys()

    def get_trace_ids(self, trace_type):
        return self.traces_per_type[trace_type]

    def get_trace(self, traceid):
        if traceid not in self.type_per_trace:
            print ("Trace %s does not existss" % traceid)
            return None
        trace_path = "%s/%s/%s.%s" % (self.directory, self.type_per_trace[traceid], traceid, self.filetype)
        if not exists(trace_path):
            print ("Trace %s does not exist" % traceid)
            return None

        return Trace.loadTrace(trace_path)

    def add_file(self, folder, filename):
        folder = folder[2:] # Remove "./" from folder name
        self.type_per_trace[filename] = folder
        if folder not in self.traces_per_type:
            self.traces_per_type[folder] = []
        self.traces_per_type[folder].append(filename)
    
    def _find_files(self, subdir="."):
        current = "%s/%s" % (self.directory, subdir)
        if not exists(current):
            return
        for name in listdir(current):
            full = "%s/%s" % (current, name)
            relative = "%s/%s" % (subdir, name)
            if isdir(full):
                self._find_files(relative)
            elif isfile(full) and name.endswith("."+self.filetype):
                self.add_file(subdir, name[:name.rfind("."+self.filetype)])