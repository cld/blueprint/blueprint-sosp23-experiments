from json import load

def hash_string(str):
    """ Same hashcode as java string hashcode s[0]*31^(n-1) + s[1]*31^(n-2) + ... + s[n-1] """
    n = len(str)-1
    hc = 0
    for c in str:
        hc = (hc + ord(c)*pow(31, n, 4294967295)) % 4294967295  
        n = n - 1
    return hc


def hash_report(report):
    hc = 1
    for k in ["Agent", "Label", "ProcessName"]:
        
        # Consider Source (file:line) instead of Label
        if k == "Label" and "Source" in report:
            k = "Source"

        if k in report:
            string = report[k]
            hc = hc * pow(hash_string(k), hash_string(string), 4294967295) % 4294967295
    return hc


class Node():
    
    def __init__(self, id, label, data):
        self.id = id
        self.label = label
        self.data = data
        
    def has_field(self, fieldname):
        return fieldname in self.data
        
    def field(self, fieldname):
        if fieldname in self.data:
            return self.data[fieldname]
        return []

    @staticmethod
    def fromJSON(json):
        return Node(json["EventID"], hash_report(json), json)

class Trace:

    """
    A Trace is the representation of an X-Trace trace, and includes the reports and such
    """

    def __init__(self, id):
        self.id = id
        
        self.nodeid_to_parentids = {}
        self.nodeid_to_childids  = {}
        self.nodeid_to_label  = {}
        self.label_to_nodeids = {}
        
        self.edgeid_to_weight = {}
        self.edgeid_to_label  = {}
        self.label_to_edgeids = {}

    def add_node(self, nodeid, nodelabel):
        """ Add a node to the trace's graph """
        # Cannot add a node that has already been added
        assert nodeid not in self.nodeid_to_label
        
        # Add the mappings between node id and label
        self.nodeid_to_label[nodeid] = nodelabel
        if nodelabel not in self.label_to_nodeids:
            self.label_to_nodeids[nodelabel] = set()
        self.label_to_nodeids[nodelabel].add(nodeid)
        
        # Add empty lists for node parents and children
        self.nodeid_to_parentids[nodeid] = set()
        self.nodeid_to_childids[nodeid] = set()

    def add_edge(self, pid, cid, edgeweight):
        """ Add an edge to the trace's graph """
        # Can only add a node if the parent and child have both been added
        assert pid in self.nodeid_to_label
        assert cid in self.nodeid_to_label
        assert pid in self.nodeid_to_childids
        assert cid in self.nodeid_to_parentids
        
        # Can only add edges with weight greater than 0
        assert edgeweight > 0
        
        # Determine the edge id and label
        edgeid = (pid, cid)
        edgelabel = (self.nodeid_to_label[pid], self.nodeid_to_label[cid])
        
        # Cannot add an edge that has already been added
        assert edgeid not in self.edgeid_to_label
        
        # Add the edge
        self.edgeid_to_label[edgeid] = edgelabel
        self.edgeid_to_weight[edgeid] = edgeweight
        if edgelabel not in self.label_to_edgeids:
            self.label_to_edgeids[edgelabel] = set()
        self.label_to_edgeids[edgelabel].add(edgeid)
        
        # Link the parents and children
        self.nodeid_to_childids[pid].add(cid)
        self.nodeid_to_parentids[cid].add(pid)

    def get_node_label(self, nodeid):
        return self.nodeid_to_label[nodeid]

    def get_labels(self):
        return self.label_to_nodeids.keys()

    def get_ids(self):
        return self.nodeid_to_label.keys()

    def get_children(self, nodeid):
        return self.nodeid_to_childids[nodeid]

    def get_all_walks(self, walk_lenght):
        """ 
        Return all walks 
        """
        all_walks = []
        nodes = list(self.get_ids())

        for node in nodes:
            self.all_walks(node, walk_lenght, all_walks, [])

        return all_walks

    def all_walks(self, start_node, walk_lenght, all_walks, walk_so_far):
        """
        Return all walks from a certain node
        """
        walk_so_far = walk_so_far + [start_node]
        if len(walk_so_far) == walk_lenght:
            walk = [self.get_node_label(i) for i in walk_so_far]
            all_walks.append(walk)
            return
        
        children = self.get_children(start_node)
        
        if len(children) == 0:
            return
        
        for child in children:
            self.all_walks(child, walk_lenght, all_walks, walk_so_far)

    @staticmethod
    def loadTrace(filename):
        return Trace.fromJSON(load(open(filename))[0])

    @staticmethod
    def fromJSON(json):
        trace = Trace(json["id"])
        
        nodes = {}
        for report in json["reports"]:
            assert "EventID" in report
            node = Node.fromJSON(report)
            nodes[node.id] = node

            trace.add_node(node.id, node.label)
        
        for nodeid in nodes:
            node = nodes[nodeid]
            if node.has_field("ParentEventID"):
                for pid in node.field("ParentEventID"):
                    if pid != "0" and pid != "0000000000000000": # pid = 0 (or 0000000000000000) represents root event
                        edgeweight = max(0.000000000001, float(nodes[nodeid].data["Timestamp"]) - float(nodes[pid].data["Timestamp"]))
                        trace.add_edge(pid, node.id, edgeweight)
        
        return trace