#import tensorflow as tf
import tensorflow.compat.v1 as tf
import numpy as np

tf.disable_v2_behavior()

class Sifter:

    def __init__(self, num_traces, vocab_size, embedding_size, trace_embedding_size, window_size, num_sampled, learning_rate, concat=True):
        """ Creates the neural network model to be used by Sifter.

        Params:
            num_traces: total number of traces to be added to the model
            vocab_size: vocabulary size
            embedding_size: word embedding size
            trace_embedding_size: trace embedding size
            window_size: window size
            num_sampled: Number of negative examples to sample
            learning_rate: Leaning rate
            concat: if will use concatenation or sum to combine vectors
        """

        # Define Embeddings:
        self.embeddings = tf.Variable(tf.random_uniform([vocab_size, embedding_size], -1.0, 1.0))
        self.doc_embeddings = tf.Variable(tf.random_uniform([num_traces, trace_embedding_size], -1.0, 1.0))

        concat_size = (embedding_size * window_size + trace_embedding_size) if concat else (embedding_size + trace_embedding_size)

        # NCE loss parameters
        self.nce_weights = tf.Variable(tf.truncated_normal([vocab_size, concat_size],
                                                    stddev=1.0 / np.sqrt(concat_size)))
        self.nce_biases = tf.Variable(tf.zeros([vocab_size]))

        # Create data/target placeholders
        self.x_inputs = tf.placeholder(tf.int32, shape=[None, window_size + 1]) # plus 1 for doc index
        self.y_target = tf.placeholder(tf.int32, shape=[None, 1])
        
        self.batch_s = tf.placeholder(tf.int32, shape=(None, embedding_size))
        if concat:
            # Use concatenation to combine vectors
            self.embeds = []
            for element in range(window_size):
                self.embeds.append(tf.nn.embedding_lookup(self.embeddings, self.x_inputs[:, element]))
            self.embed = tf.concat(axis=1, values=self.embeds)
        else:
            # Use sum to combine vectors
            # Add together element embeddings in window:
            self.embed = tf.zeros(tf.shape(self.batch_s))
            for element in range(window_size):
                self.embed += tf.nn.embedding_lookup(self.embeddings, self.x_inputs[:, element])


        self.doc_indices = tf.slice(self.x_inputs, [0,window_size],tf.shape(self.y_target))
        self.doc_embed = tf.nn.embedding_lookup(self.doc_embeddings, self.doc_indices)

        # Concatenate embeddings
        self.final_embed = tf.concat(axis=1, values=[self.embed, tf.squeeze(self.doc_embed)])

        # Get loss function
        self.loss_function = self.get_loss_function(vocab_size, num_sampled)

        # Get training optimizer
        self.train_optimizer = self.get_train_optimizer(self.loss_function, learning_rate)

    def get_loss_function(self, vocabulary_size, num_sampled):
        """ Returns loss function (NCE Loss).

        Params:
            vocabulary_size: Vocabulary size
            num_sampled: Number of negative examples to sample
        """
        return tf.reduce_mean(tf.nn.nce_loss(weights=self.nce_weights,
                                     biases=self.nce_biases,
                                     labels=self.y_target,
                                     inputs=self.final_embed,
                                     num_sampled=num_sampled,
                                     num_classes=vocabulary_size))

    def get_train_optimizer(self, loss, model_learning_rate):
        """ Returns gradient descent optimizer to be used in the training step.
        
            Params:
                loss: Loss function to be used in the training step
                model_learning_rate: Leaning rate
        """
        optimizer = tf.train.GradientDescentOptimizer(learning_rate=model_learning_rate)
        return optimizer.minimize(loss)
