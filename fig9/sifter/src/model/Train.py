import numpy as np
from model.Data import get_context_from_walks

def train(model, walks, trace_id, session, num_epochs, embedding_size):
    """ Train the model with a trace and return the loss

    Params:
        model: neural network model
        walks: list of trace walks
        trace_id: trace id
        train_step: train function
        loss: loss function
        num_epochs: number of epochs to train the model
        embedding_size: embedding size
        session: tensorflow session

    Returns:
        Loss average for all epochs
    """
    
    batch_data, labels_data = get_context_from_walks(walks, trace_id)
    batch_inputs = np.array(batch_data)
    batch_labels = np.transpose(np.array([labels_data]))
    batch_size_shape = [[0]*embedding_size]*len(batch_data)

    loss_vec = []
    for i in range(num_epochs):

        feed_dict = {model.x_inputs : batch_inputs, model.y_target : batch_labels, model.batch_s : batch_size_shape}

        # Run the train step
        session.run(model.train_optimizer, feed_dict=feed_dict)

        # Calculate loss
        loss_val = session.run(model.loss_function, feed_dict=feed_dict)
        loss_vec.append(loss_val)

    return np.mean(loss_vec)