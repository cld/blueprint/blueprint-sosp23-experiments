from fileio.Experiment import Experiment

def build_dictionary_using_vocabulary(vocabulary):
    """
        Build dictionary for the vocabulary of all traces events. 
        Each label for an event in a trace will be represented as a number. 

        Params:
            vocabulary

        Returns:
            word_dict: dictionary for the vocabulary 
    """
    c = 0
    word_dict = {}
    for word in vocabulary:
        word_dict[word] = c
        c += 1
    return(word_dict)

def walks_to_numbers(traces, word_dict):
    """
        For each trace walks, change label by the number in the dictionary

        Params:
            traces: list of trace walks
            word_dict: dictionary representing each event label as a number

        Returns:
            data
    """
    data = []
    for t in traces:
        trace_data = []
        for walk in t:
            sentence_data = []
            
            for word in walk:
                if word in word_dict:
                    word_ix = word_dict[word]
                else:
                    word_ix = 0
                sentence_data.append(word_ix)
            trace_data.append(sentence_data)
        data.append(trace_data)
    return(data)    

def get_context_from_walks(walks, trace_id):
    """
        Returns context for each walk. 

        Params:
            walks: List of walks for a trace
            trace_id: Trace ID
    """
    batch_and_labels = []
    for walk in walks:
        middle = int(len(walk)/2)
        context = walk[:middle] +  walk[middle+1:] + [trace_id]
        batch_and_labels.append( (context, walk[middle]) )
    batch, labels = [list(x) for x in zip(*batch_and_labels)]

    return(batch, labels)

def load_traces_data(experiment, walk_size=5):
    """
        Load traces and return a dict containing, for each trace type, all traces walks.

        Params:
            experiment: Experiment containing all traces 
            walk_size: Lenght of the walk

        Returns:
            traces_data: All traces walks separeted by trace type
            Size of the vocabulary, considering all events labels for all traces
            num_traces: Total number of traces
    """
    
    vocabulary = set()
    traces_walks = {}
    traces_types = experiment.get_trace_types()

    for t in traces_types:
        traces_walks[t] = []
        for g_id in experiment.get_trace_ids(t):
            trace = experiment.get_trace(g_id)
            walks = trace.get_all_walks(walk_size)
            traces_walks[t].append(walks)

            nodes = trace.get_labels()
            vocabulary |= set(nodes)
        

    word_dictionary = build_dictionary_using_vocabulary(vocabulary)
    traces_data = {}
    num_traces = 0
    for t in traces_types:
        traces_data[t] = walks_to_numbers(traces_walks[t], word_dictionary)
        traces_data[t] = walks_to_numbers(traces_walks[t], word_dictionary)
        num_traces += len(traces_walks[t])

    return traces_data, len(vocabulary), num_traces
    