#!/bin/bash

mkdir -p ../ae_data/blueprint-dsb-sn-xtrace/normal
mkdir -p ../ae_data/blueprint-dsb-sn-xtrace/error

for i in {1..10}
do
    python trace_splitter.py ../ae_data/compose_normal_${i}.json ../ae_data/blueprint-dsb-sn-xtrace/normal
done
python trace_splitter.py ../ae_data/compose_error.json ../ae_data/blueprint-dsb-sn-xtrace/error