import sys
import json
import os

def main():
    if len(sys.argv) != 3:
        print("Usage: python trace_splitter.py input_file output_dir")
        sys.exit(1)

    fname = sys.argv[1]
    out_dir = sys.argv[2]
    with open(fname, 'r') as inf:
        data = json.load(inf)
        for d in data:
            id = d['id']
            with open(os.path.join(out_dir, id + '.json'), 'w') as outf:
                json.dump([d], outf, indent=4)

if __name__ == '__main__':
    main()
