# 6.3 Reproducible Research - Sifter

In this experiment, we reproduce figure 9 from the accepted version of the paper.

## Prerequisites

Before you run any experiments for figure 9, we would like to remind you to setup your environment and machines according to the information in the Kick-the-tires Readme.

## Generating + Running Systems

To generate the system, on $MACHINE2, run the [generate_sn.sh](./scripts/generate_sn.sh) script in the scripts directory.

Execute the script as follows

```
> ./scripts/generate_sn.sh
```

To deploy the system, on $MACHINE2, execute the following steps.

```
> cd $BLUEPRINT_SYSTEMS_DIR/blueprint-dsb-socialnetwork/output_go_sospae_fig8_repl
> sudo docker compose build
> sudo docker compose up container1 container2 container3 container4 container5 container6 container7 container8 container9 container10 container11 container12 container13 container14 container15 container16 container17
# Wait for 30 seconds to 1 minute
> sudo docker compose up -d
```

Run the script called [init_social_graph.sh](../common/init_social_graph.sh) from $MACHINE1 to populate the user database.

To run the workload, on $MACHINE1, run the [run_exp.sh](./scripts/run_exp.sh) script in the scripts directory.

Execute the instructions as follows

```
> cd scripts
> ./run_exp.sh
```
### Downloading traces

You will need to download 2 different sets of traces from the x-trace server. First set of traces will be non-error type ComposePost traces. You will need around 1000 such traces for this experiment.
The second set of traces will be error-type ComposePost traces. You will only need 5 such traces for this experiment.

Both of these sets of traces can be downloaded from the xtrace server that will be running on $MACHINE2_IP:4080. To download the normal set of traces, select the set of traces you want and click on the Get Selected Json button as shown in the Figure. This will open a new browser window that will contain the raw json. Copy the raw json and paste it in a file in the ae_data directory called ```compose_normal_{file_num}.json``` where file_num denotes how many such tracing files have been downloaded. You can download upto 100 traces in 1 go so you will need to create 10 files containing the raw traces. The x-trace screen should look like this:

![normal](./blueprint-fig9-ae_2.png)

Once you have these files, follow the same procedure for the error traces. You can quickly find error traces by searching for Error in the search bar at the top of the xtrace screen. Make sure you select the tag option from the dropdown menu. Put the raw json in the ae_data directory in a file called ```compose_error.json```.
Here is what the xtrace screen might look like:

![error](./bllueprint-fig9-ae_1.png)

After downloading the traces, your ae_data directory will have the following files: `compose_normal_1.json`, `compose_normal_2.json`, `compose_normal_3.json`, `compose_normal_4.json`, `compose_normal_5.json`, `compose_normal_6.json`, `compose_normal_7.json`, `compose_normal_8.json`, `compose_normal_9.json`, `compose_normal_10.json`, `compose_error.json`

Run the [prepare_traces](./scripts/prepare_traces.sh) to prepare the data to be used by Sifter.

### Running Sifter

After following the previous steps, you can run an extracted, packaged version of Sifter by simply running the jupyter notebook titled ```Sifter Example - Sampling anomalies``` in the sifter/src folder. This will generate a file called `dsb-compose-probabilities.csv` in the ae_data folder that can then be used by the `Results` jupyter notebook to recreate the graph.

## Plotting Results

To plot the newly generated results, we have provided a Jupyter notebook. Currently the Jupyter notebook uses the data that we had generated for the original experiments. To view the generated results, modify the variable called `DATA_DIR` such that its value is now `ae_data/` instead of `data/`. After this, you can re-run all the cells in the Jupyter notebook.