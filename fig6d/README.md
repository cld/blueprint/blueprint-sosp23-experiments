# 6.2.1 Type 4 Metastability Failure

In this experiment, we reproduce figure 6d from the accepted version of the paper.

## Prerequisites

Before you run any experiments for figure 6d, we would like to remind you to setup your environment and machines according to the information in the Kick-the-tires Readme.

## Generating + Running Systems

To generate the system, on $MACHINE2, run the [generate_sn.sh](./scripts/generate_sn.sh) script in the scripts directory.

Execute the script as follows

```
> ./scripts/generate_sn.sh
```

To deploy the system, on $MACHINE2, execute the following steps.

```
> cd $BLUEPRINT_SYSTEMS_DIR/blueprint-dsb-socialnetwork/output_go_sospae_fig6d
> sudo docker compose build
> sudo docker compose up -d
```

Run the script called [init_social_graph.sh](../common/init_social_graph.sh) from $MACHINE1 to populate the user database.

To run the workload, on $MACHINE1, run the [meta4.sh](./scripts/meta4.sh) script in the scripts directory.

Execute the instructions as follows

```
> cd scripts
> ./meta4.sh
```

To get the cache measurement logs from $MACHINE2, execute the following command

```
> sudo docker logs container28 &> meta4_cache_miss_rate_3000.txt
```

For proper analysis, you must transfer this file from $MACHINE2 to $MACHINE1 and place it in this experiment's ae_data folder

## Plotting Results

To plot the newly generated results, we have provided a Jupyter notebook. Currently the Jupyter notebook uses the data that we had generated for the original experiments. To view the generated results, modify the variable called `DATA_DIR` such that its value is now `ae_data/` instead of `data/`. After this, you can re-run all the cells in the Jupyter notebook.