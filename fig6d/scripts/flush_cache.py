from pymemcache.client import base
import sys

def main():
    address = sys.argv[1]
    client = base.Client((address, 11211))
    client.flush_all()


if __name__ == '__main__':
    main()
