#!/bin/bash

source ../../ae_ktr/env.sh
mkdir -p ../ae_data

config=socialnetwork_blueprint_ReadUserTimeline
python fill_cache.py $MACHINE2_IP $MACHINE2_IP
$BLUEPRINT_DIR/wrk_http -config=../../common/$1.json -tput=3000 -duration=2m -outfile=../ae_data/meta4_$1_3000.csv &
sleep 60
python flush_cache.py $MACHINE2_IP
