from pymemcache.client import base
from pymongo import MongoClient
import sys
import json

def main():
    db_address = sys.argv[1]
    cache_address = sys.argv[2]

    db_client = MongoClient(db_address, 27017)
    cache_client = base.Client((cache_address, 11211))
    db = db_client.post
    collection = db["post"]
    cursor = collection.find({})
    for document in cursor:
        #print(document)
        del document['_id']
        cache_client.set(str(document["postid"]), json.dumps(document))

if __name__ == '__main__':
    main()
