#!/bin/bash

source ../ae_ktr/env.sh

echo "Backend,Interface,Compiler"
var="$(wc -l < $BLUEPRINT_DIR/stdlib/components/cache.go)"
echo "Cache,$var,0"
var="$(wc -l < $BLUEPRINT_DIR/stdlib/components/nosqldb.go)"
echo "NoSQLDB,$var,0"
var="$(wc -l < $BLUEPRINT_DIR/stdlib/components/reldb.go)"
echo "RelDB,$var,0"
var="$(wc -l < $BLUEPRINT_DIR/stdlib/components/queue.go)"
echo "Queue,$var,0"
var="$(wc -l < $BLUEPRINT_DIR/stdlib/components/tracer.go)"
echo "Tracer,$var,0"
var="$(wc -l < $BLUEPRINT_DIR/generators/deploy/core_deployer_interface.go)"
var2="$(wc -l < $BLUEPRINT_DIR/generators/deploy/core_deployer.go)"
echo "Deployer,$var,$var2"
var="$(wc -l < $BLUEPRINT_DIR/generators/netgen/core_net_interface.go)"
var2="$(wc -l < $BLUEPRINT_DIR/generators/plugin_rpc.go)"
echo "RPC,$var,$var2"
var="$(wc -l < $BLUEPRINT_DIR/generators/netgen/core_net_interface.go)"
var2="$(wc -l < $BLUEPRINT_DIR/generators/plugin_web.go)"
echo "HTTP,$var,$var2"