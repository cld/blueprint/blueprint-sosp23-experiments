# 6.5 Adding new backend interfaces

To re-generate Table 2, simply run the included script [print_table.sh](./print_table.sh). Upon running the script, you should
see the following output

```
Backend,Interface,Compiler
Cache,11,0
NoSQLDB,28,0
RelDB,21,0
Queue,11,0
Tracer,44,0
Deployer,7,64
RPC,16,167
HTTP,16,160
```

Note that RPC and HTTP use a common interface.
The lines of code don't exactly match the Table included in the paper as there have been improvements in the code since the SOSP submission deadline.