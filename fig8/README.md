# 6.2.2 Cross-System Inconsistency

In this experiment, we reproduce figure 8 from the accepted version of the paper.

## Prerequisites

Before you run any experiments for figure 8, we would like to remind you to setup your environment and machines according to the information in the Kick-the-tires Readme.

## Generating + Running Systems

We use two different variants of dsb-sn for this experiment.

### Variant 1: No Replication

To generate the system, on $MACHINE2, run the [generate_sn_norepl.sh](./scripts/generate_sn_norepl.sh) script in the scripts directory.

Execute the script as follows

```
> ./scripts/generate_sn.sh
```

To deploy the system, on $MACHINE2, execute the following steps.

```
> cd $BLUEPRINT_SYSTEMS_DIR/blueprint-dsb-socialnetwork/output_go_sospae_fig8_no_repl
> sudo docker compose build
> sudo docker compose up -d
```

Run the script called [init_social_graph.sh](../common/init_social_graph.sh) from $MACHINE1 to populate the user database.

To run the workload, on $MACHINE1, run the [run_norep.sh](./scripts/run_norep.sh) script in the scripts directory.

Execute the instructions as follows

```
> cd scripts
> ./run_rep.sh
```

### Variant 2: Replicated Database

To generate the system, on $MACHINE2, run the [generate_sn_repl.sh](./scripts/generate_sn_repl.sh) script in the scripts directory.

Execute the script as follows

```
> ./scripts/generate_sn.sh
```

To deploy the system, on $MACHINE2, execute the following steps.

```
> cd $BLUEPRINT_SYSTEMS_DIR/blueprint-dsb-socialnetwork/output_go_sospae_fig8_repl
> sudo docker compose build
> sudo docker compose up container1 container2 container3 container4 container5 container6 container7 container8 container9 container10 container11 container12 container13 container14 container15 container16
# Wait for 15-30 seconds
> sudo docker exec container4 /rs-init-dbrs.sh # Apply the replica set configuration to the mongo databases
> sudo docker compose up -d
```

Run the script called [init_social_graph.sh](../common/init_social_graph.sh) from $MACHINE1 to populate the user database.

To run the workload, on $MACHINE1, run the [run_rep.sh](./scripts/run_rep.sh) script in the scripts directory.

Execute the instructions as follows

```
> cd scripts
> ./run_rep.sh
```

## Plotting Results

To plot the newly generated results, we have provided a Jupyter notebook. Currently the Jupyter notebook uses the data that we had generated for the original experiments. To view the generated results, modify the variable called `DATA_DIR` such that its value is now `ae_data/` instead of `data/`. After this, you can re-run all the cells in the Jupyter notebook.