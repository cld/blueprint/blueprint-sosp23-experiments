from pymemcache.client import base
from pymongo import MongoClient
import sys
import json

def main():
    db_address = sys.argv[1]
    cache_address = sys.argv[2]

    db_client = MongoClient(db_address, 27017)
    cache_client = base.Client((cache_address, 11211))
    #db = db_client["user-timeline"]
    #collection = db["user-timeline"]
    #collection.drop()
    cache_client.flush_all()

if __name__ == '__main__':
    main()
