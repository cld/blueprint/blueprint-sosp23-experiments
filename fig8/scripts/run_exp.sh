#!/bin/bash

source ../../ae_ktr/env.sh
config=$1
mkdir -p ../ae_data
for i in {100..100..100}
do
	for j in {0..1000..100}
	do
		wait_dur="${j}ms"
		echo "Running $i $j"
		outfile="../ae_data/xsi_compose_${config}_${i}_${j}ms.csv"
		$BLUEPRINT_DIR/xsconsistency -wait=$wait_dur -tput=$i -outfile=$outfile -addr=http://$MACHINE2_IP:9000
		python clear_ds.py $MACHINE2_IP $MACHINE2_IP
		sleep 1m
	done
done
