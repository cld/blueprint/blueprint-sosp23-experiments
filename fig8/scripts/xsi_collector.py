import pandas as pd
import sys

def main():
    if len(sys.argv) != 3:
        print("Usage: python xsi_collector.py <input_prefix> <outfile>")
        sys.exit(1)
    prefix = sys.argv[1]
    outfile = sys.argv[2]
    with open(outfile, 'w') as outf:
        outf.write("Wait,Inconsistency_percent\n")
        for i in [0,1,2,3,4,5,6,7,8,9,10]:
            filename = prefix + str(i*100) + "ms.csv"
            df = pd.read_csv(filename)
            total_rows = len(df.index)
            filtered_df = df[df.Found == False]
            num_inconsistencies = filtered_df.shape[0] / total_rows
            outf.write(str(i) + "," + str(num_inconsistencies) + "\n")


if __name__ == '__main__':
    main()
