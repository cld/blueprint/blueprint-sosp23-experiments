# 6.1 Reducing Implementation Effort

Before running these scripts, please ensure that the environment variable ```BLUEPRINT_SYSTEMS_DIR``` is correctly set in the [env.sh](../ae_ktr/env.sh) file.

To re-generate table 1, first download all the necessary systems using the [download_systems.sh](./download_systems.sh) script as follows:

```
> ./download_systems.sh
```

Next, you can regenerate the table using the script called [./gen_tab1.sh](./gen_tab1.sh) as follows:

```
> ./gen_tab1.sh
```

After the script executes, you should see the following output

```
System    Orig.(LoC)  Spec    Wiring  Reduction
DSB SocialNetwork 13538 1472 59 8.8
DSB Media 11974 1439 63 8.0
DSB HotelReservation 5160 731 41 6.7
TrainTicket 54002 9673 165 5.5
SockShop 13995 2329 39 5.9
```

Note, that the original numbers of the DSB SocialNetwork and DSB Media systems are different as we hadn't counted all the source code lines correctly in the submitted version of the paper. Some of the spec and wiring numbers have changed as the blueprint compiler has matured and undergone changes which have had to be reflected in the wiring and spec implementations of the paper.