#!/bin/bash

source ../ae_ktr/env.sh
mkdir -p $BLUEPRINT_SYSTEMS_DIR
cd $BLUEPRINT_SYSTEMS_DIR
echo "Downloading TrainTicket"
git clone git@github.com:FudanSELab/train-ticket.git
cd train-ticket
git checkout 350f62000e6658e0
echo "Downloaded TrainTicket"

cd $BLUEPRINT_SYSTEMS_DIR
echo "Downloading SockShop"
mkdir -p sockshop
cd sockshop
git clone git@github.com:microservices-demo/carts.git
cd carts
git checkout 2616b7f767b232
cd ../
git clone git@github.com:microservices-demo/catalogue.git
cd catalogue
git checkout e9e5338599dbd
cd ../
git clone git@github.com:microservices-demo/front-end.git
cd front-end
git checkout 14254f98c0fffd
cd ../
git clone git@github.com:microservices-demo/orders.git
cd orders
git checkout dc58fe271a2bb6
cd ../
git clone git@github.com:microservices-demo/payment.git
cd payment
git checkout 8ae49056c42a745
cd ../
git clone git@github.com:microservices-demo/queue-master.git
cd queue-master
git checkout cc7562cc9d138637e
cd ../
git clone git@github.com:microservices-demo/shipping.git
cd shipping
git checkout 7797af5958ef6fbc3
cd ../
git clone git@github.com:microservices-demo/user.git
cd user
git checkout ea7bc23723af8452c
echo "Downloaded SockShop"

cd $BLUEPRINT_SYSTEMS_DIR
echo "Downloading DeathStarBench"
git clone git@github.com:delimitrou/DeathStarBench.git
cd DeathStarBench
git checkout 8323f9c3b109a
echo "Downloaded DeathStarBench"

cd $BLUEPRINT_SYSTEMS_DIR
echo "Downloading Blueprint-systems"
git clone https://gitlab.mpi-sws.org/cld/blueprint/systems/blueprint-dsb-hotelreservation.git
git clone https://gitlab.mpi-sws.org/cld/blueprint/systems/blueprint-dsb-media.git
git clone https://gitlab.mpi-sws.org/cld/blueprint/systems/blueprint-dsb-socialnetwork.git
git clone https://gitlab.mpi-sws.org/cld/blueprint/systems/blueprint-sockshop.git
git clone https://gitlab.mpi-sws.org/cld/blueprint/systems/blueprint-trainticket.git
echo "Downloaded Blueprint-systems"
