import os
import sys

def count_hotel_lines():
    blueprint_sys_dir = os.environ["BLUEPRINT_SYSTEMS_DIR"]
    original_lines = 0
    orig_dir = os.path.join(blueprint_sys_dir, "DeathStarBench", "hotelReservation")
    INIT_LINES_OFFSET = 455 # These are the number of lines that are needed for populating the databases. These aren't pertinent to the system so they should be excluded from the calculations
    for root, dirs, files in os.walk(orig_dir):
        if "vendor" in root:
            continue
        for f in files:
            if not f.endswith(".java") and not f.endswith(".go") and not f.endswith(".js"):
                continue
            with open(os.path.join(root, f)) as inf:
                original_lines += len(inf.readlines())

    total_lines = 0
    sys_dir = os.path.join(blueprint_sys_dir, "blueprint-dsb-hotelreservation", "input", "input_go", "services")
    for file in os.listdir(sys_dir):
        with open(os.path.join(sys_dir, file)) as inf:
            total_lines += len(inf.readlines())

    original_lines = original_lines - INIT_LINES_OFFSET
    total_lines = total_lines - INIT_LINES_OFFSET

    wiring_lines = 0
    wiring_dir = os.path.join(blueprint_sys_dir, "blueprint-dsb-hotelreservation", "wiring")
    with open(os.path.join(wiring_dir, "instances.py")) as inf:
        wiring_lines = len(inf.readlines())
    print("DSB HotelReservation", original_lines, total_lines, wiring_lines, round(original_lines / (total_lines + wiring_lines), 1))

def count_sn_lines():
    blueprint_sys_dir = os.environ["BLUEPRINT_SYSTEMS_DIR"]
    original_lines = 0
    orig_dir = os.path.join(blueprint_sys_dir, "DeathStarBench", "socialNetwork")
    for root, dirs, files in os.walk(orig_dir):
        if "gen-cpp" in root or "gen-lua" in root or "gen-py" in root or "wrk2" in root or "third-party" in root:
            continue
        for f in files:
            if not f.endswith(".lua") and not f.endswith(".cpp") and not f.endswith(".h") and not f.endswith(".hpp"):
                continue
            with open(os.path.join(root, f)) as inf:
                original_lines += len(inf.readlines())

    total_lines = 0
    sys_dir = os.path.join(blueprint_sys_dir, "blueprint-dsb-socialnetwork", "input_v2", "input_go", "services")
    for file in os.listdir(sys_dir):
        with open(os.path.join(sys_dir, file)) as inf:
            total_lines += len(inf.readlines())

    wiring_lines = 0
    wiring_dir = os.path.join(blueprint_sys_dir, "blueprint-dsb-socialnetwork", "wiring")
    with open(os.path.join(wiring_dir, "instances_v2.py")) as inf:
        wiring_lines = len(inf.readlines())
    print("DSB SocialNetwork", original_lines, total_lines, wiring_lines, round(original_lines / (total_lines + wiring_lines), 1))

def count_media_lines():
    blueprint_sys_dir = os.environ["BLUEPRINT_SYSTEMS_DIR"]
    original_lines = 0
    orig_dir = os.path.join(blueprint_sys_dir, "DeathStarBench", "mediaMicroservices")
    for root, dirs, files in os.walk(orig_dir):
        if "gen-cpp" in root or "gen-lua" in root or "gen-py" in root or "wrk2" in root or "third-party" in root:
            continue
        for f in files:
            if not f.endswith(".lua") and not f.endswith(".cpp") and not f.endswith(".h") and not f.endswith(".hpp"):
                continue
            with open(os.path.join(root, f)) as inf:
                original_lines += len(inf.readlines())

    total_lines = 0
    sys_dir = os.path.join(blueprint_sys_dir, "blueprint-dsb-media", "input", "input_go", "services")
    for file in os.listdir(sys_dir):
        with open(os.path.join(sys_dir, file)) as inf:
            total_lines += len(inf.readlines())

    wiring_lines = 0
    wiring_dir = os.path.join(blueprint_sys_dir, "blueprint-dsb-media", "wiring")
    with open(os.path.join(wiring_dir, "instances.py")) as inf:
        wiring_lines = len(inf.readlines())
    print("DSB Media", original_lines, total_lines, wiring_lines, round(original_lines / (total_lines + wiring_lines), 1))

def count_sockshop_lines():
    blueprint_sys_dir = os.environ["BLUEPRINT_SYSTEMS_DIR"]
    original_lines = 0
    orig_dir = os.path.join(blueprint_sys_dir, "sockshop")
    for root, dirs, files in os.walk(orig_dir):
        for f in files:
            if not f.endswith(".java") and not f.endswith(".go") and not f.endswith(".js"):
                continue
            with open(os.path.join(root, f)) as inf:
                original_lines += len(inf.readlines())

    total_lines = 0
    sys_dir = os.path.join(blueprint_sys_dir, "blueprint-sockshop", "input", "input_go", "services")
    for file in os.listdir(sys_dir):
        with open(os.path.join(sys_dir, file)) as inf:
            total_lines += len(inf.readlines())

    wiring_lines = 0
    wiring_dir = os.path.join(blueprint_sys_dir, "blueprint-sockshop", "wiring")
    with open(os.path.join(wiring_dir, "instances.py")) as inf:
        wiring_lines = len(inf.readlines())
    print("SockShop", original_lines, total_lines, wiring_lines, round(original_lines / (total_lines + wiring_lines), 1))

def count_trainticket_lines():
    blueprint_sys_dir = os.environ["BLUEPRINT_SYSTEMS_DIR"]
    original_lines = 0
    orig_dir = os.path.join(blueprint_sys_dir, "train-ticket")
    for root, dirs, files in os.walk(orig_dir):
        if "ts" in root:
            for f in files:
                if not f.endswith(".java"):
                    continue
                with open(os.path.join(root, f), encoding='utf8') as inf:
                    original_lines += len(inf.readlines())

    total_lines = 0
    sys_dir = os.path.join(blueprint_sys_dir, "blueprint-trainticket", "input", "input_go", "services")
    for file in os.listdir(sys_dir):
        with open(os.path.join(sys_dir, file)) as inf:
            total_lines += len(inf.readlines())

    wiring_lines = 0
    wiring_dir = os.path.join(blueprint_sys_dir, "blueprint-trainticket", "wiring")
    with open(os.path.join(wiring_dir, "instances.py")) as inf:
        wiring_lines = len(inf.readlines())
    print("TrainTicket", original_lines, total_lines, wiring_lines, round(original_lines / (total_lines + wiring_lines), 1))

def main():
    if len(sys.argv) != 2:
        print("Usage: python count_lines.py [system_name]")
        sys.exit(1)

    system = sys.argv[1]
    if system == 'hotel':
        count_hotel_lines()
    elif system == 'social-network':
        count_sn_lines()
    elif system == 'media':
        count_media_lines()
    elif system == 'sockshop':
        count_sockshop_lines()
    elif system == 'train-ticket':
        count_trainticket_lines()
    else:
        print("Unknown system")
        sys.exit(2)

if __name__ == '__main__':
    main()